import { expect } from 'expect';

describe('Array', function () {
    it('should not contain the number four', function () {
        expect([1, 2, 3]).not.toContain(4);
    });
});
